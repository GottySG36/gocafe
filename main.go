// Simple utility to compile and track coffee usage

package main

import (
	"bufio"
    "crypto/sha256"
	"flag"
	"fmt"
    "golang.org/x/crypto/ssh/terminal"
	"log"
	"math/rand"
	"os"
	"strings"
    "syscall"
	"time"
)

var (
    // Use the following to set at compile time :
    // go build -ldflags "-X main.<variable>=<value>" main.go
    salt string = "X37&&T!75Sex"
    defaultDir string = "./coffee-info.tsv"
)

const LayoutLog = "Mon, 02 Jan 2006 15:04:05"
const LayoutComp = "2-Jan-2006"

var Codes = [...]string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
                        "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#", "@", "$", "%", "*", "=",
                        "+"}

// func LoadData(name string) (map[string]string, int, error) {
//
//     count := 0
//     lines := make(map[string]string)
//     // Add check for file extension?
//     inFile, err := os.Open(name)
//     defer inFile.Close()
//     if err != nil {
//         return nil, 0, err
//     }
//
//     n := bufio.NewScanner(inFile)
//     for n.Scan() {
//         data := strings.Split(n.Text(), "\t")
//         lines[data[0]] = data[1]
//         if data[1] != "-" {
//             count++
//         }
//     }
//
//     return lines, count, nil
// }

// func WriteData(name string, lines map[string]string) error {
//
//     outf, err := os.Create(name)
//     defer outf.Close()
//     if err != nil {
//         return err
//     }
//     w := bufio.NewWriter(outf)
//     for date, char := range lines {
//         fmt.Fprintf(w, "%v\t%v\n", date, char)
//     }
//     w.Flush()
//     return nil
// }

func NewCoffee(name string, own bool, pwd string) error {
	var code string = "-"
	if own {
		code = Codes[rand.Intn(len(Codes))]
	}
	date := time.Now().Format(LayoutLog)
    hash := hashPasswd(pwd, date, code)

	newEntry := fmt.Sprintf("%v\t%v\t%v\n", date, code, hash)

	fmt.Fprintf(os.Stdout, "%v", newEntry)

	// If the file doesn't exist, create it, else append to the file
	f, err := os.OpenFile(name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer f.Close()
	if err != nil {
		return err
	}
	if _, err := f.Write([]byte(newEntry)); err != nil {
		return err
	}

	return nil
}

func TimeFrame(name, from, to string) ([][3]string, int, error) {
	count := 0
	b_date, err := time.Parse(LayoutComp, *begin)
	if err != nil {
		return nil, 0, err
	}
	e_date, err := time.Parse(LayoutComp, *end)
	if err != nil {
		return nil, 0, err
	}
    e_date = e_date.Add(time.Hour *  24 - time.Second * 1)

	var lines [][3]string

	inFile, err := os.Open(name)
	defer inFile.Close()
	if err != nil {
		return nil, 0, err
	}

	n := bufio.NewScanner(inFile)
	for n.Scan() {
		data := strings.Split(n.Text(), "\t")
		date, err := time.Parse(LayoutLog, data[0])
		if err != nil {
			return nil, 0, err
		}

		if date.After(b_date) && date.Before(e_date) {
            lines = append(lines, [3]string{data[0], data[1], data[2]})
			if data[1] != "-" {
				count++
			}
		}
	}

	return lines, count, nil
}

func PromptPwd() (string, error) {
    fmt.Printf("Passkey : ")
    fmt.Printf("\n")
    passwd, err := terminal.ReadPassword(int(syscall.Stdin))
    if err != nil {
        return "", err
    }
    return string(passwd), nil
}

func hashPasswd(passwd, date, code string) string {
    sum := sha256.Sum256([]byte(passwd+salt+date+code))
    return fmt.Sprintf("%x", sum)
}


var add = flag.Bool("a", false, "Adds a new entry to the table")
var begin = flag.String("b", "01-Jan-2019", "Timeframe start date (format DD-Mon-YYYY)")
var end = flag.String("e", "31-Dec-2999", "Timeframe end date (format DD-Mon-YYYY)")
var outp = flag.String("o", defaultDir, "Output file used to store the data")
var pwd = flag.Bool("p", false, "Prompt for a passwd")
var printHash = flag.Bool("ph", false, "Also prints the hash values")
var show = flag.Bool("s", false, "Prints the stored data and exits")
var own = flag.Bool("w", false, "Use this option if you are using the lab's coffee, not your own")

func main() {
	rand.Seed(time.Now().UnixNano())
	flag.Parse()

    var passwd string
    var err error
    if *pwd {
        passwd, err = PromptPwd()
        if err != nil {
            log.Fatalf("Error -:- %v\n", err)
        }
    }

	if *add {
		if err := NewCoffee(*outp, *own, passwd); err != nil {
			log.Fatalf("Error -:- %v\n", err)
		}
	}

	if *show  {

		data, nb, err := TimeFrame(*outp, *begin, *end)
		if err != nil {
			log.Fatalf("Error -:- %v\n", err)
		}

		b_date, err := time.Parse(LayoutComp, *begin)
		if err != nil {
			log.Fatalf("Error -:- %v\n", err)
		}
		e_date, err := time.Parse(LayoutComp, *end)
		if err != nil {
			log.Fatalf("Error -:- %v\n", err)
		}
        e_date = e_date.Add(time.Hour *  24 - time.Second * 1)
		fmt.Fprintf(os.Stdout, "\nFrom\t: %v\nTo\t: %v\n__________________________________________________\n", b_date, e_date)

		fmt.Fprint(os.Stdout, "Date\tCode\n")
        for _, value := range data {
            if *printHash {
                state := value[2] == hashPasswd(passwd, value[0], value[1])
                fmt.Fprintf(os.Stdout, "%v\t%v\t%v\t%v\n", value[0], value[1], state, value[2])
            } else {
                fmt.Fprintf(os.Stdout, "%v\t%v\n", value[0], value[1])
            }
		}

		fmt.Fprintf(os.Stdout, "\n__________________________________________________\nCoffee count\n - Home\t: %v\n - Lab\t: %v\n", len(data)-nb, nb)

	}
}
