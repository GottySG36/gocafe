module bitbucket.org/GottySG36/gocafe

go 1.15

require (
	golang.org/x/crypto v0.0.0-20221005025214-4161e89ecf1b
	golang.org/x/sys v0.0.0-20221006211917-84dc82d7e875 // indirect
	golang.org/x/term v0.0.0-20220919170432-7a66f970e087 // indirect
)
